<?php 
  // Template Name: Página Lista de Lobinhos
?>

<?php get_header(); ?>
  
  <main>
    <section class="example">
      <div class="container">

        <div class="searchContainer">
          <input class="searchWolf" type="search" name="" id="" placeholder="Digite a busca aqui...">
          <a href="../Adicionar Lobinho/addWolfePage.html" class="btnSearchWolf"> + Lobo </a href="../Adicionar Lobinho/addWolfePage.html"><br>
          
          <input class="checkSearchWolf" type="checkbox" name="" id="">
          <span class="checkText">Ver lobinhos adotados</span>     
        </div>                        
      </div>

    <?php 
      $the_query = new WP_Query('posts_per_page=2');
    ?>

    <?php 
      //
    ?>

    <?php 
      if ( have_posts() ) : while ( have_posts() ) : the_post(); 
    ?>

    <?php 
      // $num = 0 
      // if($num % 2 == 0){ 
    ?>

    </section>
      <div class="linha1">
        <div class="exampleImgContainer">
          <?php if( get_field('lobo_imagem') ): ?>                     
            <img class="exampleImg" src="<?php the_field('lobo_imagem'); ?>" alt="">
          <?php endif; ?>
        </div>                
        <div class="exampleInfoContent">
            <div class="exampleNameContainer">
                <h3 class="exampleName"><?php the_field('lobo_nome'); ?></h3>
            </div>
            <div class="exampleAgeContainer">
                <p class="exampleAge"><?php the_field('lobo_idade'); ?></p>
            </div>
            <div class="exampleTextContainer">
                <p class="exampleText mBt70">
                    <?php the_field('lobo_descricao'); ?>
                </p>
            </div>
        </div>
      </div>
      <?php 
        // }
        // else{ 
      ?>
      <div class="linha2">            
          <div class="exampleImgContainer">
              <?php if( get_field('lobo_imagem') ): ?>                     
                  <img class="exampleImg" src="<?php the_field('lobo_imagem'); ?>" alt="">
              <?php endif; ?>
          </div>
          <div class="exampleInfoContent">
              <div class="exampleNameContainer">
                  <h3 class="exampleName"><?php the_field('lobo_nome'); ?></h3>
              </div>
              <div class="exampleAgeContainer">
                  <p class="exampleAge"><?php the_field('lobo_idade'); ?></p>
              </div>
              <div class="exampleTextContainer2">
                  <p class="exampleText2">
                      <?php the_field('lobo_descricao'); ?>
                  </p>
              </div>
          </div>
      </div>
      <?php 
        // }
        // $num++;
      ?>
      

      <?php endwhile; else: ?>
        <p>desculpe, o post não segue os critérios escolhidos</p>
      <?php endif; ?>

      <?php my_pagination(); ?>

  </main>

  <?php get_footer(); ?>