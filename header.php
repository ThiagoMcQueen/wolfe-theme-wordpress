<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/Styles/styleGrid.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/Styles/styleHome.css">

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/Styles/styleListaDeLobinhosGrid.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/Styles/styleListaDeLobinhos.css">

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/Styles/aboutUsStyle.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/Styles/style.css">

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/Styles/header e footer padrão/defaultStyleGrid.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/Styles/defaultStyleHeaderAndFooter.css">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@400;700&display=swap" rel="stylesheet">
    <title><?php bloginfo('name') ?></title>
    <?php wp_head(); ?>
</head> 
<body>
  <header class="navigation_bar">

      <div class="container">
  
        <div class="row justify_between">
                                     
            <div class="menu gap_menu justify_between_lg wLg100 alignCenter">
              <?php
                $args = array(
                  'menu' => 'nevegação',
                  'container' => true
                );
                wp_nav_menu($args);
              ?>                      
        </div>
  
      </div>
  
  </header>