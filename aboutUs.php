<?php 
  // Template Name: Página Quem Somos
?>

<?php get_header(); ?>

  <main class="AboutUs">
    <p class="title"> <?php the_title(); ?> </p>
    <div class="textAboutUs"> <?php the_content(); ?>  </div>        
  </main>
  
<?php get_footer(); ?>