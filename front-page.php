<?php 
  // Template Name: Página inicial
?>

<?php get_header(); ?>
  <main>

    <section class="header">      
      <div class="container">

        <div class="headerContainer">

          <div class="headerTitleContainer">
            <h2 class="headerTitle"><?php the_field('titulo_inicial'); ?></h2>
          </div>

          <div class="headerTextContainer">
            <p class="headerText">           
            <?php the_field('descricao_inicial'); ?>
            </p>
          </div>
          
        </div>

      </div>
    </section>

    <section class="about">
      <div class="container">

        <div>

          <div class="aboutTitleContainer">
            <h2 class="aboutTitle"><?php the_field('titulo_sobre'); ?></h2>
          </div>

          <div class="aboutTextContainer">
            <p class="aboutText"> 
            <?php the_field('descricao_sobre'); ?>
            </p>
          </div>

        </div>

      </div>
    </section>

    <section class="values">
      <div class="container">          

        <div class="valuesTitleContainer">
          <h2 class="valuesTitle"><?php the_field('valores_titulo_valor'); ?></h2>
        </div>

        <div class="valuesItems row rowNoWrapLg justify_center gap30">

          <div class="item">

            <div>
              <img class="subItemImg marginBtLg" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/insurance.png" alt="">
            </div>
              <h3 class="subItemTitle marginBtLg">
                <?php the_field('valores_titulo_valor_1'); ?>
              </h3>
              <p class="subItemText">
                <?php the_field('valores_descricao_valor_1'); ?>
              </p>
            

          </div>

          <div class="item">

            <div>
              <img class="subItemImg marginBtLg" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/care.png" alt="">
            </div>
              <h3 class="subItemTitle marginBtLg">
                <?php the_field('valores_titulo_valor_2'); ?>
              </h3>       
              <p class="subItemText">
                <?php the_field('valores_descricao_valor_2'); ?>
              </p>           

          </div>

          <div class="item">

            <div>
              <img class="subItemImg marginBtLg" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/frame.png" alt="">
            </div>   
              <h3 class="subItemTitle marginBtLg">
                <?php the_field('valores_titulo_valor_3'); ?>
              </h3> 
              <p class="subItemText">
                <?php the_field('valores_descricao_valor_3'); ?>
              </p>
          

          </div>

          <div class="item">

            <div>
              <img class="subItemImg marginBtLg" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/rescue.png" alt="">
            </div>
              <h3 class="subItemTitle marginBtLg">
                <?php the_field('valores_titulo_valor_4'); ?>
              </h3>
              <p class="subItemText itemText">
                <?php the_field('valores_descricao_valor_4'); ?>
              </p>    

          </div>

        </div>        

      </div>
    </section>

    <section class="example" >
        
        

        <div class="container containerExample">

            <div class="exampleTitleContainer">
                <h2 class="exampleTitle">Lobos Exemplo</h2>
            </div>
            
            <?php 
                $the_query = new WP_Query('posts_per_page=2');
            ?>

            <?php $num = 0 ?>

            <?php 
                while($the_query -> have_posts()) : $the_query -> the_post();
            ?>

            <?php if($num % 2 == 0){ ?>

            <div class="linha1">
                <div class="exampleImgContainer">
                <?php if( get_field('lobo_imagem') ): ?>                     
                    <img class="exampleImg" src="<?php the_field('lobo_imagem'); ?>" alt="">
                <?php endif; ?>
                </div>                
                <div class="exampleInfoContent">
                    <div class="exampleNameContainer">
                        <h3 class="exampleName"><?php the_field('lobo_nome'); ?></h3>
                        
                    </div>
                    <div class="exampleAgeContainer">
                        <p class="exampleAge"><?php the_field('lobo_idade'); ?></p>
                    </div>
                    <div class="exampleTextContainer">
                        <p class="exampleText mBt70">
                            <?php the_field('lobo_descricao'); ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php }
            else{ ?>

            <div class="linha2">            
                <div class="exampleImgContainer">
                    <?php if( get_field('lobo_imagem') ): ?>                     
                        <img class="exampleImg2" src="<?php the_field('lobo_imagem'); ?>" alt="">
                    <?php endif; ?>
                </div>
                <div class="exampleInfoContent">
                    <div class="exampleNameContainer2">
                        <h3 class="exampleName2"><?php the_field('lobo_nome'); ?></h3>
                    </div>
                    <div class="exampleAgeContainer">
                        <p class="exampleAge2"><?php the_field('lobo_idade'); ?></p>
                    </div>
                    <div class="exampleTextContainer2">
                        <p class="exampleText2">
                            <?php the_field('lobo_descricao'); ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php }
                $num++;
            ?>
            <?php 
                endwhile;
                wp_reset_postdata();
            ?>
        </div>
        
    </section>

  </main>

  <?php get_footer(); ?>